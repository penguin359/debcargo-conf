Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: encoding_rs
Upstream-Contact: Henri Sivonen <hsivonen@hsivonen.fi>
Source: https://github.com/hsivonen/encoding_rs

Files: *
Copyright:
 2013-2023 Henri Sivonen <hsivonen@hsivonen.fi>
 2015-2023 Mozilla Foundation
 WHATWG (Apple, Google, Mozilla, Microsoft).
License: (Apache-2.0 or MIT) and BSD-3-Clause and CC0
Notes:
 The non-test code that isn't generated from the WHATWG data in this crate is
 under Apache-2.0 OR MIT. Test code is under CC0.
.
 This crate contains code/data generated from WHATWG-supplied data. The WHATWG
 upstream changed its license for portions of specs incorporated into source
 code from CC0 to BSD-3-Clause between the initial release of this crate and the
 present version of this crate. The in-source licensing legends have been
 updated for the parts of the generated code that have changed since the
 upstream license change.

Files: debian/*
Copyright:
 2018-2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2018 Sylvestre Ledru <sylvestre@debian.org>
 2018-2019 Wolfgang Silbermayr <wolfgang@silbermayr.at>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

License: CC0
 On Debian systems, the complete text of the CC0 Public Domain Dedication can 
 be found in /usr/share/common-licenses/CC0-1.0
